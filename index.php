<?require_once 'src/autoload.php';

global $APPLICATION, $DB;

$DB = new \App\DB();
$APPLICATION = new \App\Boot();
$APPLICATION->start();
