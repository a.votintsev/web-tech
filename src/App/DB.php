<?php
namespace App;

use PDO;

class DB {
    private $DB;

    public function __construct() {
        $opt = [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES => false,
        ];
        $this->DB = new PDO('mysql:host=localhost;dbname=web_tech', 'root', '', $opt);
    }

    public function getListMovies() {
        return $this->DB->query('SELECT id, name, KP, IMDB, image FROM movies')->fetchAll();
    }

    public function getMovieById(int $id) {
        return $this->DB->query('SELECT id, name, description, year, image FROM movies WHERE id=' . $id)->fetch();
    }

    public function ifNotExist($movieName) {
        $movieName = trim($movieName); // удаляет лишние пробелы
        $movieName = htmlspecialchars($movieName); // заменяет спецсимволы html
        $movieName = addslashes($movieName); // экранирует кавычки и спецсимволы
        return empty($this->DB->query('SELECT id FROM movies WHERE name="' . $movieName . '"')->fetch());
    }

    public function add($data) {
        $stmt = $this->DB->prepare("INSERT INTO movies (name, year, image, KP, IMDB) VALUES (:name, :year, :image, :KP, :IMDB)");
        $stmt->bindParam(':name', $name);
        $stmt->bindParam(':year', $year);
        $stmt->bindParam(':image', $image);
        $stmt->bindParam(':KP', $KP);
        $stmt->bindParam(':IMDB', $IMDB);
        $name = $data['name'];
        $year = $data['year'];
        $image = $data['image'];
        $KP = (empty($data['KP'])) ? null : $data['KP'];
        $IMDB = (empty($data['IMDB'])) ? null : $data['IMDB'];

        return $stmt->execute();
    }
}