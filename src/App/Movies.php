<?php
namespace App;

use DOMDocument;
use DOMXPath;

class Movies {
    private $html;
    private $movies = [];

    /**
     * Movies constructor.
     */
    private function __construct() {
        $useragent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.89 Safari/537.36';
        $timeout = 120;
        $dir = dirname(__FILE__);
        $cookie_file = $dir . '/cookies/' . md5($_SERVER['REMOTE_ADDR']) . '.txt';

        $ch = curl_init('https://c.greenfilm.vip/filmy-5/');
        curl_setopt($ch, CURLOPT_FAILONERROR, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_file);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_file);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true );
        curl_setopt($ch, CURLOPT_ENCODING, "" );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt($ch, CURLOPT_AUTOREFERER, true );
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout );
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout );
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10 );
        curl_setopt($ch, CURLOPT_USERAGENT, $useragent);
        curl_setopt($ch, CURLOPT_REFERER, 'http://www.google.com/');
        $this->html = html_entity_decode(curl_exec($ch));
    }

    /**
     * Получает информацию о фильмах из HTML
     */
    private function parseHTML() {
        $dom = new DOMDocument();
        $dom->loadHTML($this->html);
        $xpath = new DOMXPath($dom);
        $items = $xpath->query("//div[@class='th-item']");

        foreach ($items as $item) {
            $this->movies[] = [
                'name' => $xpath->query(".//div[@class='th-title']", $item)->item(0)->nodeValue,
                'image' => $item->getElementsByTagName('img')->item(0)->getAttribute('src'),
                'KP' => $xpath->query(".//div[@class='th-rate th-rate-kp']", $item)->item(0)->nodeValue,
                'IMDB' => $xpath->query(".//div[@class='th-rate th-rate-imdb']", $item)->item(0)->nodeValue,
                'year' => $xpath->query(".//div[@class='th-series']", $item)->item(0)->nodeValue,
                'detail_page_link' => $item->getElementsByTagName('a')->item(0)->getAttribute('href'),
            ];
        }
    }

    /**
     * Возвращает массив с информацией о фильмах
     *
     * @return array
     */
    public static function getInfo() {
        $ob = new Movies();
        $ob->parseHTML();

        return $ob->movies;
    }
}