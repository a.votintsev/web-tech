<?php
include 'autoload.php';
$movies = \App\Movies::getInfo();
$db = new \App\DB();

$rules = [
    'name' => 'required|string',
    'year' => 'required|string',
    'image' => 'required|string',
    'KP' => 'numeric',
    'IMDB' => 'numeric',
];

foreach ($movies as $movie) {
    if($db->ifNotExist($movie['name'])) {
        $validator = \Services\Validator::make($movie, $rules);
        if(!$validator->fails()) {
            $db->add($movie);
        }
    }
}

