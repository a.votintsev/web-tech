<?global $APPLICATION, $DB?>
<?$APPLICATION->setTitle('Детальная страница');?>
<?$APPLICATION->addCss('/css/detail.css');?>

<?$data = $DB->getMovieById($_GET['id'])?>
<div class="detail-page">
    <div class="container px-5">
        <h1 class="py-5"><?=$data['name']?></h1>
        <div class="row">
            <div class="col-2">
                <img src="<?=$data['image']?>" alt="banner-1" class="img-responsive">
            </div>
            <div class="col-9">
                <div class="text">
                    <p><strong>Год выхода: </strong><span><?=$data['year']?></span></p>
                    <p><?=$data['description']?></p>
                </div>
            </div>
        </div>
    </div>
</div>
