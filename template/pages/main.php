<?global $APPLICATION, $DB?>
<?$APPLICATION->setTitle('Главная');?>
<?$APPLICATION->addCss('/css/main.css');?>

<div class="main-container">
    <div class="container py-5">
        <div class="row px-4">
            <?foreach ($DB->getListMovies() as $movie) {?>
                <a href="/detail?id=<?=$movie['id']?>" class="col-lg-4 col-md-4 col-sm-4 col-xs-12 p-4">
                    <div class="movie-card">
                        <img src="<?=$movie['image']?>" alt="banner-1" class="img-responsive">
                        <div class="description">
                            <h2 class="name"><?=$movie['name']?></h2>
                            <div class="row">
                                <div class="col-6"><span class="KP">КП</span> <?=$movie['KP']?></div>
                                <div class="col-6"><span class="IMDB">IMDB</span> <?=$movie['IMDB']?></div>
                            </div>
                        </div>
                    </div>
                </a>
            <?}?>
        </div>
    </div>
</div>